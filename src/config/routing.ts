export function routingConfig($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            template: require("../views/examples.html")
        })
        .otherwise({
            redirectTo: "/"
        });
}

routingConfig.$inject = ["$routeProvider", "$locationProvider"];