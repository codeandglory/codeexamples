export class ExampleTypescriptController {
    public name: String = "";
    public greeting: String = "";

    public sayHello(): void {
        this.greeting = `Hello, ${this.name}!`;
    }
}