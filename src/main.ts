import "angular";
import "angular-translate";
import "angular-route";
import {ExampleTypescriptController} from "./controllers/ExampleTypescriptController";
import {ExampleDirective} from "./directives/ExampleDirective";
import {routingConfig} from "./config/routing";

var appModule: ng.IModule = angular.module("com.axxes.masterclass.codeexamples", ["ngRoute"])
    .controller("ExampleTypescriptController", () => new ExampleTypescriptController())
    .directive("exampleDirective", () => new ExampleDirective())
    .constant("CONSTANTS", {
        foo: "bar"
    }).config(routingConfig);

angular.bootstrap(document, [appModule.name], {
    strictDi: true
});